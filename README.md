# Calculo quinconcio

Faz o calculo de pontos de plantio no formato [Quincioncio](http://agronomiarustica.com/sistema-de-plantio-em-quinconcio/)
Gerando um arquivo de texto delimitado por ";" (ponto e vírgula), que deve ser adicionado ao [Qgis](www.qgis.org).

## Changelog
-  2019-03-15
    - Criação de camada virtual com tabela de conteúdos usando o SRC da camada de origem.
    - Modificando o tipo dos campos da tabela de atributos "linha" e "ponto" para inteiro.
    - Criação de camada virtual com o mesmo nome do arquivo TXT - se ele for criado.
-  2019-03-13
    - Criação do plugin.

## TODO
  * Gerar camada de ponto com seus atributos direto no Qgis - Feito em 2019-03-15
  * Addicionar qualquer arquivo vetorial ao qgis (ou chamar o pŕoprio gerenciador de dados do QGIS).
  * Filtrar os pontos que estão dentro da camada utilizada e a camada apenas com tais pontos.
  * Salvar a camada em outros formatos de arquivos.
  * Metodos de cálculo para aproveitar melhor a área a ser plantada.
  * Adicionar conversão de Sistema de Referência de Coordenadas - SRC.
  * Adicionar outros sistemas de plantio.
  * Tradução para outras linguas.
  * Melhorar wiki.



# Autor
[Kyle Felipe Vieira Roberto](www.kylefelipe.com)
