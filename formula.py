# -*- coding: utf-8 -*-
u"""verificando se uma matriz quadrada é triangular superior ou inferior."""

from math import sqrt

lado = 3
area = 152985.63

x_min, y_min, x_max, y_max = (226298.3591495389, 7910540.118207952,
                              227203.08269267605, 7911174.868518971)


def altura(lado):
    """Calcula a distancia entre as linhas."""
    h = (lado*sqrt(3))/2
    return h


def densidade(area, lado):
    u"""Calculando a densidade de árvores."""
    d = int(area/(lado*altura(lado)))
    return d


def qt_linhas(y_max, y_min, lado):
    u"""Quantidade de qt_linhas Fórmula: (y_maior-y_menor)/h."""
    u"""h = altura(lado)"""
    qt = int((y_max - y_min)/altura(lado))+1
    return qt


def qt_colunas(x_max, x_min, lado):
    u"""Quantidade de colunas."""
    qt = int((x_max - x_min)/lado)+1
    return qt


def gera_pontos(x_min, y_min,  colunas, linhas, altura, lado):
    u"""Quantidade de pontos por linha."""
    pts = []
    s = 1
    for l in range(0, linhas):
        linha = []
        y = y_min + (len(pts)*altura)

        for c in range(0, colunas):
            if s == 1:
                p = [x_min + (c * lado), y]
            if s == -1:
                p = [x_min + (lado/2) + (c * lado), y]
            linha.append(p)
        s = s * -1
        pts.append(linha)
    return pts


def gera_csv(pontos, output='pontos_gerados.txt'):
    """Gerando CSV de pontos."""
    arquivo = """linha;ponto;x;y\n"""
    for linha in pontos:
        for ponto in linha:
            x, y = ponto
            # arquivo = arquivo + '\n'
            linha_ = "{};{};{};{}\n".format(pontos.index(linha)+1,
                                            linha.index(ponto)+1, x, y)
            arquivo = arquivo + linha_
            # with open('pontos_gerados.csv', 'a') as file:
            #     file.write(arquivo)

    with open(output, 'w') as file:
        file.write(arquivo)
    return arquivo


# alt = altura(lado)
# qt_lin = qt_linhas(y_max=y_max, y_min=y_min, lado=lado)
# qt_col = qt_colunas(x_max, x_min, lado)
# pts_gerados = gera_pontos(x_min, y_min, qt_col, qt_lin, alt, lado)
# gera_csv(pts_gerados)
#
# print(alt)
# print(qt_lin)
# print(qt_col)
