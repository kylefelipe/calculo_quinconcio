# This file contains metadata for your plugin. Since
# version 2.0 of QGIS this is the proper way to supply
# information about a plugin. The old method of
# embedding metadata in __init__.py will
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Cálculo Quincôncio
qgisMinimumVersion=3.0
description=Calculates the individuals' points at planting using Quincôncio system.
version=0.1
author=Kyle Felipe Vieira Roberto
email=kylefelipe@gmail.com


about=Calculates the individuals' points at planting using Quincôncio system. Need to set the size of the edge and choose a layer to calculate the points coordinates. A new virtual layer will be created using chosen layer's extent. See hompage to how to use...

tracker=https://gitlab.com/kylefelipe/calculo_quinconcio/issues
repository=https://gitlab.com/kylefelipe/calculo_quinconcio
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
changelog=0.1 (2016-03-16)
   - Start of plugin.

# Tags are comma separated with spaces allowed
tags=point, quinconcio, planting

homepage=https://gitlab.com/kylefelipe/calculo_quinconcio/wikis/home
category=Vector
icon=quinconcio.png
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False
